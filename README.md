#StatBundle

The StatBundle provides Analytics for the Integrated project.

##Documentation

The documentation is stored in the Resources/doc/ folder.

[Read the Documentation](https://bitbucket.org/peter_stage/statbundle/src/master/Resources/doc/)

##Installation

The installation instructions can be found in the documentation.

##About

The StatBundle is part of the Integrated project.