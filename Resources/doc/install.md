### Installing the StatBundle

**Step 1:** Install Integrated.

**Step 2:** Fork this repository into your Integrated installation.

**Step 3:** Register the Statbundle.

	//~\app\AppKernel.php

	public function registerBundles()
    {
        $bundles = array(
			...
            new Stage\StatBundle\StageStatBundle(),
        );

**Step 4:** Edit the Solr mapping in `~\integrated\content-bundle\Integrated\Bundle\ContentBundle\Resources\config\solr\mapping.yml` to include the taxonomy items in your system.
	
#### Example
	
Single: 

`facet_news_category: $.getReferencesByRelationType("news_category")[].title.value()`
	
Multiple:

`facet_keyword: $.getReferencesByRelationType("keyword_main" && "keyword_sub")[].title.value()`
	
**Step 5:** Run the following commands:
	
	php app/console solr:indexer:full-index 
	php app/console solr:indexer:run -f
	 
**Step 6:** Edit your `~StatBundle\Controller\ContentAnalyticsController.php` to use the correct taxonomy items in the $taxis array in the getTaxItems and getTaxItemsByType functions.

**Step 7:** Add the following code to the assets section of `~\app\config\config.yml` :

	statbundle_js:
        inputs:
           - @StageStatBundle/Resources/public/js/*
    statbundle_css:
        inputs:
           - @StageStatBundle/Resources/public/css/*
	
##### Additional note
Since we have implemented morris.js locally it also has to be updated manually.
###### In case of an update:
**Step 1:** Download the newest version from the [official site](http://www.oesmith.co.uk/morris.js/)

**Step 2:** Replace the version in `~\StatBundle\Resources\public\js\morris.js`

**Step 3:** Change "xLabelAngle" to 40.