<?php

namespace Stage\StatBundle\Tests\Controller;

use Stage\StatBundle\Controller\ContentAnalyticsController;
use Symfony\Component\HttpFoundation\Request;

class DefaultControllerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var $cactrl ContentAnalyticsController
     */
    private $cactrl;

    /**
     * @var $request Request
     */
    private $request;

    public function setUp()
    {
        $this->cactrl = new ContentAnalyticsController();
        $this->request = new Request();

//        $this->request->attributes->set('sd','2014-04-01T00:00:00.000Z');
//        $this->request->attributes->set('ed','2014-05-31T23:59:59.099Z');
//        $this->request->attributes->set('ctype','contenttype');
    }

    public function testIndexAction()
    {
        $this->assertEquals(0, count($this->cactrl->indexAction()));
    }

    public function testJsonAction()
    {
        $this->request->attributes->set('sd','invalid');
        $this->request->attributes->set('ed','invalid');
        $this->assertNull(null, $this->cactrl->JsonAction($this->request));
    }

    public function testDetailAction()
    {
        $this->request->attributes->set('sd','invalid');
        $this->request->attributes->set('ed','invalid');
        $this->request->attributes->set('ctype','taxonomy');
        $this->assertNull(null, $this->cactrl->DetailAction($this->request));
        $this->assertArrayHasKey('type', $this->cactrl->DetailAction($this->request));
        $this->assertArrayHasKey('typename', $this->cactrl->DetailAction($this->request));
        $this->assertArrayHasKey('slug', $this->cactrl->DetailAction($this->request));
        $this->assertArrayHasKey('title', $this->cactrl->DetailAction($this->request));


        $this->request->attributes->set('ctype','contenttype');
        $this->assertNull(null, $this->cactrl->DetailAction($this->request));
        $this->assertArrayHasKey('type', $this->cactrl->DetailAction($this->request));
        $this->assertArrayHasKey('typename', $this->cactrl->DetailAction($this->request));
        $this->assertArrayHasKey('slug', $this->cactrl->DetailAction($this->request));
        $this->assertArrayHasKey('title', $this->cactrl->DetailAction($this->request));
    }

    public function testDetailJsonAction()
    {
        $this->request->attributes->set('sd','invalid');
        $this->request->attributes->set('ed','invalid');
        $this->assertNull(null, $this->cactrl->detailJsonAction($this->request));
    }

    public function testExtraAction()
    {
        $this->request->attributes->set('sd','invalid');
        $this->request->attributes->set('ed','invalid');
        $this->assertNull(null, $this->cactrl->ExtraAction($this->request));
        $this->assertArrayHasKey('ctypes', $this->cactrl->ExtraAction($this->request));
        $this->assertArrayHasKey('taxitems', $this->cactrl->ExtraAction($this->request));
        $this->assertArrayHasKey('status', $this->cactrl->ExtraAction($this->request));
        $this->assertArrayHasKey('typename', $this->cactrl->ExtraAction($this->request));
        $this->assertArrayHasKey('slug', $this->cactrl->ExtraAction($this->request));
    }

    public function testDetailExtraAction()
    {
        $this->request->attributes->set('sd','invalid');
        $this->request->attributes->set('ed','invalid');
        $this->request->attributes->set('ctype','contenttype');
        $this->assertNull(null, $this->cactrl->detailextraAction($this->request));
        $this->assertArrayHasKey('ctypes', $this->cactrl->detailextraAction($this->request));
        $this->assertArrayHasKey('taxitems', $this->cactrl->detailextraAction($this->request));
        $this->assertArrayHasKey('status', $this->cactrl->detailextraAction($this->request));
        $this->assertArrayHasKey('typename', $this->cactrl->detailextraAction($this->request));
        $this->assertArrayHasKey('slug', $this->cactrl->detailextraAction($this->request));
    }

    public function testTaxDetailExtraAction()
    {
        $this->request->attributes->set('sd','invalid');
        $this->request->attributes->set('ed','invalid');
        $this->assertNull(null, $this->cactrl->taxdetailextraAction ($this->request));
        $this->assertArrayHasKey('items', $this->cactrl->taxdetailextraAction ($this->request));
        $this->assertArrayHasKey('status', $this->cactrl->taxdetailextraAction ($this->request));
        $this->assertArrayHasKey('typename', $this->cactrl->taxdetailextraAction ($this->request));
        $this->assertArrayHasKey('itemname', $this->cactrl->taxdetailextraAction ($this->request));
        $this->assertArrayHasKey('slug', $this->cactrl->taxdetailextraAction ($this->request));
    }

    public function testGetPerc()
    {
        $this->assertEquals(0.0, $this->cactrl->getPerc(2,0));
        $this->assertEquals(0.0, $this->cactrl->getPerc(0,2));
        $this->assertEquals(50.0, $this->cactrl->getPerc(1,2));
        $this->assertEquals(0.0, $this->cactrl->getPerc(-1,2));
        $this->assertEquals(0.0, $this->cactrl->getPerc(1,-2));
        $this->assertEquals(0.0, $this->cactrl->getPerc('hoi', 'hallo'));
        $this->assertEquals(0.0, $this->cactrl->getPerc(1, 'hallo'));
    }

    public function testValidateDate()
    {
        $this->assertEquals(false, $this->cactrl->validateDate('12/03/2004'));
        $this->assertEquals(false, $this->cactrl->validateDate(null));
        $this->assertEquals(false, $this->cactrl->validateDate('0000-00-00T00:00:00.000Z'));
        $this->assertEquals(true, $this->cactrl->validateDate('2014-04-12T22:00:00.000Z'));
    }
}