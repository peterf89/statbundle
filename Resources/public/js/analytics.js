/******
 *
 * Fires when the document is ready
 * Warning message displaying "JS is disabled" gets removed
 * Text fields get set to current date and 1 month ago from current date
 * Graphs are created for the daterange: 1 month ago-today
 *
 */
jQuery( document ).ready(function() {
    jQuery("#jscheck").html('');
    jQuery("#jscheck").removeClass();
    var d,
        eindDatum,
        startDatum;

    // Clear datepicker values
    jQuery("#inputStart").val('');
    jQuery("#inputStop").val('');

    // Set date range automatically to the past month
    d = new Date();
    d.setHours(23,59,59,999);
    eindDatum = d.toISOString();
    jQuery("#inputStop").attr("placeholder", getTimeString(d));
    d.setHours(0,0,0,0);
    d.setMonth(d.getMonth() - 1);
    startDatum = d.toISOString();
    jQuery("#inputStart").attr("placeholder", getTimeString(d));

    // Show graph for the past month
    showGraph(startDatum, eindDatum);
});

/******
 *
 * Fires when the user clicks within the dateClick div. Toggles visibility
 * for the datepicker and calls showGraph on close.
 *
 */
jQuery(".dateClick").click(function(){
    jQuery("#dateRef").toggle("slow", function() {
        if(jQuery("#dateRef").is(":hidden"))
        {
            var start = jQuery("#inputStart").val() || '',
                stop = jQuery("#inputStop").val() || '';
            if(start != '' && stop == '')
            {
                jQuery("#inputStop").val(jQuery("#inputStart").val());
                stop = start;
                jQuery('#dateRef').datepicker("option", "minDate", null);
            }
            if((start && stop) != '')
            {
                startDatum = new Date(start);
                startDatum.setHours(2,0,0,0);
                eindDatum = new Date(stop);
                eindDatum.setHours(25,59,59,999);
                showGraph(startDatum.toISOString(), eindDatum.toISOString());
            }
        }
    });
})

/******
 *
 * function showGraph: create 2 graphs (donut and bar) for the given range.
 * @param startDatum
 * @param eindDatum
 */

function showGraph(startDatum, eindDatum)
{
    var titel;
    // Show loader image
    jQuery('#content').html('');
    jQuery('#content').addClass('loader');

    // Check if this is the mainpage or a detail page.
    var json = 'json/?sd=' + startDatum + '&ed=' + eindDatum;

    if(jQuery("#selected").val() != undefined){
        titel = jQuery("#selected").val();
    }

    if(titel != "Overige") //No graphs are drawn on an extra(Overige) page
    {
        jQuery.getJSON( json, function( data ) {
            if(data[0].result == 'OK')
            {
                var taxonomy = false,
                    count = 0;
                if(titel == undefined) //Current page is homepage
                {
                    var cTypes = data[0].contenttypes;
                    var taxonomies = data[0].taxonomies;

                    if(cTypes.length > 0)
                    {
                        createGraph('Content Types', cTypes, startDatum, eindDatum);
                        count++;
                    }
                    else if(count=0)
                    {
                        jQuery("#content").removeClass('loader');
                        jQuery("#content").append('<div class="alert alert-info clearfloat">No Content Types were found.</div>');
                    }

                    // Taxonomy stuff:
                    if(taxonomies)
                    {
                        taxonomy = true;
                        jQuery.each(taxonomies, function(index, tax) {
                            createGraph(index, tax.items, startDatum, eindDatum, taxonomy);
                            count++;
                        });
                    }
                    else if(count=0)
                    {
                        jQuery("#content").removeClass('loader');
                        jQuery("#content").append('<div class="alert alert-info clearfloat">No Taxonomy Items were found.</div>');
                    }
                }
                else //Current page is a detail page
                {
                    var items = data[0].items;
                    var type = data[0].type;

                    if(type == 'taxonomy')
                    {
                        if(items.length > 0)
                        {
                            createGraph('Content Types', items, startDatum, eindDatum, taxonomy, true);
                            count++;
                        }
                        else if(count=0)
                        {
                            jQuery("#content").removeClass('loader');
                            jQuery("#content").append('<div class="alert alert-info clearfloat">No Content Types were found.</div>');
                        }
                    }
                    else if(type == 'contenttype')
                    {
                        taxonomy = true;
                        jQuery.each(items, function(index, tax) {
                            if(tax.items.length>0)
                            {
                                createGraph(index, tax.items, startDatum, eindDatum, taxonomy, true);
                                count++;
                            }
                            else if(count=0)
                            {
                                jQuery("#content").removeClass('loader');
                                jQuery("#content").append('<div class="alert alert-info clearfloat">No Taxonomy Items were found.</div>');
                            }
                        });
                    }
                }
            }
            else
            {
                jQuery("#content").removeClass('loader');
                jQuery("#content").html('<div class="alert alert-info clearfloat">No results were found. Please adjust your search options and try again. </div>');
            }
        });
    }
}

/*****
 *
 * Create the 2 graphs needed for showGraph()
 * @param graphName
 * @param Types
 * @param startDatum
 * @param eindDatum
 * @param taxonomy
 * @param detail
 */
function createGraph(graphName, Types, startDatum, eindDatum, taxonomy, detail)
{
    taxonomy = taxonomy || false;
    detail = detail || false;

    var title = graphName.replace('facet_','');

    var content = '<div id="' + graphName + '" class="tax_item">' +
        '<h2 class="title">' + title + '</h2><hr>' +
        '   <div id="' + graphName + '_donut" class="donutgraph"></div>' +
        '   <div id="' + graphName + '_bar" class="bargraph"></div>' +
        '<div id="' + graphName + '_item_list"></div></div>';

    var donutData = [],
        barData = [],
        titel;

    jQuery("#content").removeClass('loader');
    jQuery("#content").append(content);

    if(jQuery("#selected").val() != undefined){
        titel = jQuery("#selected").val();
    }

    Types.sort(function(a, b){
        if(a.count < b.count){
            return -1;
        }
        else if(a.count > b.count){
            return 1;
        }
        return 0;
    });

    jQuery.each(Types, function(key, value) {
        var text = beautify(value.title);
        barData.push({ 'label' : text, 'value' : value.count });
        donutData.push({ 'label' : beautify(value.title, 25), 'value' : value.perc });
    });
    createDonut(graphName, donutData, Types, detail, taxonomy, startDatum, eindDatum);
    createBar(graphName, barData, Types, detail, taxonomy, startDatum, eindDatum);
}

/*****
 *
 * Create a donut graph needed for createGraph()
 * @param graphName
 * @param donutData
 * @param Types
 * @param detail
 * @param taxonomy
 * @param startDatum
 * @param eindDatum
 */
function createDonut(graphName, donutData, Types, detail, taxonomy, startDatum, eindDatum)
{
    Morris.Donut({
        element: graphName + '_donut',
        data: donutData,
        formatter: function (x) { return x + "%";}
    }).on('click', function(i, row){
        var t = Types[i].title;
        if(t == "Overige")
        {
            if(detail)
            {
                t = 'overige/?sd=' + startDatum + '&ed=' + eindDatum;
                if(taxonomy)
                {
                    t += '&tax=' + graphName.toLowerCase();
                }
            }
            else
            {
                t = 'overige/?sd=' + startDatum + '&ed=' + eindDatum;
            }
            document.location.href = t;
        }
        if(taxonomy && !detail)
        {
            document.location.href = 'taxonomy/' + graphName.toLowerCase() + '/' + t;
        }
        else if(!taxonomy && !detail)
        {
            document.location.href = 'contenttype/' +  t;
        }
    });
}

/*****
 *
 * Create a bar graph needed for createGraph()
 * @param graphName
 * @param barData
 * @param Types
 * @param detail
 * @param taxonomy
 * @param startDatum
 * @param eindDatum
 */
function createBar(graphName, barData, Types, detail, taxonomy, startDatum, eindDatum)
{
    Morris.Bar({
        element: graphName + '_bar',
        data: barData,
        xkey: ['label'],
        ykeys: ['value'],
        labels: ['Aantal'],
        hoverCallback: function(index, options, content) {
            return(Types[index].count);
        }
    }).on('click', function(i, bar){
        var t = Types[i].title;
        if(t == "Overige")
        {
            if(detail)
            {
                t = 'overige/?sd=' + startDatum + '&ed=' + eindDatum;
                if(taxonomy)
                {
                    t += '&tax=' + graphName.toLowerCase();
                }
            }
            else
            {
                t = 'overige/?sd=' + startDatum + '&ed=' + eindDatum;
            }
            document.location.href = t;
        }
        if(taxonomy && !detail)
        {
            document.location.href = 'taxonomy/' + graphName.toLowerCase() + '/' + t;
        }
        else if(!taxonomy && !detail)
        {
            document.location.href = 'contenttype/' +  t;
        }
    });
}

/******
 *
 *  function beautify: Trim string to limit and make it pretty
 *  @param: string
 *  @param: limit (optional)
 *
 ******/
function beautify(string, limit)
{
    // If limit is not passed, set to default.
    limit = limit || 10;

    // Cut off string at limit if necessary.
    if(string.length > limit){
        string = string.substr(0,(limit - 1)) + '...';
    }

    // Remove underscores and set every first letter to uppercase.
    string = string.replace(/_/g, ' ');
    string = string.replace(/\w\S*/g, function(string){return string.charAt(0).toUpperCase() + string.substr(1);});
    return string;
}

/*******
 *
 * Adds a zero to a number lower than 10 so the date follows the format of yyyy-mm-dd
 * @param x
 * @returns (string) x
 */
function addZero(x)
{
    if (x < 10)
    {
        x = '0' + x
    }
    return x;
}

/*******
 *
 * Returns a string with the needed date so it can be displayed in a textfield in the correct format
 * @param date
 * @returns {string} timeString
 */
function getTimeString(date)
{
    var year,
        month,
        day,
        timeString;

    year = date.getFullYear();
    month = date.getMonth() + 1;
    day = date.getDate();
    timeString = year + '-' + addZero(month) + '-' + addZero(day);

    return timeString;
}