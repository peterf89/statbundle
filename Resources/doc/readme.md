####Introduction

*Content Analytics* or *StatBundle* was created to display statistics for the Integrated Content Bundle in a clear, clean manner.
This was achieved by using the Morris.js component for graphs.

####Description of functionality

This bundle allows you to view information about articles created in the Content Bundle for a selected date range. You can change
this range using the datepicker in the upper right corner. The datepicker relies on *jQuery UI*. 

The skewed labels in the graphs are made possible by an edit in the morris.js file. Therefore morris.js had to be implemented locally
instead of using the CDN.

####How to start

See install.md for detailed installation instructions.

####Examples

To display the Bar and Donut graphs for a single item you can call the `createGraph()` function:

`createGraph(index, tax.items, startDatum, eindDatum, taxonomy, true);`

