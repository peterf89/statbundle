/*
 * jQuery UI Datepicker: Using Datepicker to Select Date Range
 * http://salman-w.blogspot.com/2013/01/jquery-ui-datepicker-examples.html
 */
$( document ).ready(function() {
    $("#dateRef").datepicker({
        numberOfMonths: 3,
        changeMonth: true,
        changeYear: true,
        maxDate: 0,
        showButtonPanel: true,
        beforeShowDay: function(date) {
            var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#inputStart").val());
            var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#inputStop").val());
            return [true, date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)) ? "dp-highlight" : ""];
        },
        onSelect: function(dateText, inst) {
            var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#inputStart").val());
            var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#inputStop").val());
            if (!date1 || date2) {
                $("#inputStart").val(dateText);
                $("#inputStop").attr("placeholder", '');
                $("#inputStop").val("");
                $(this).datepicker("option", "minDate", dateText);
            } else {
                $(".dateClick").trigger('click');
                $("#inputStop").val(dateText);
                $(this).datepicker("option", "minDate", null);
            }
        }
    });
    $.datepicker.setDefaults({
        dateFormat: 'yy-mm-dd'
    });
});